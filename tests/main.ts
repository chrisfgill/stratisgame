const assert = require("chai").assert;

const src = ["map", "name", "npc"];
let tests = [];

src.forEach(e => {
    tests.push(require("./"+e));
});

tests.forEach(e => {
    e.test(assert);
});