/// grabs and sets mechanicals to db from players and planets

import Dynamo = require("aws-sdk/clients/dynamodb");
import AWS = require("aws-sdk");

const CONFIG = require("./config.json").aws;
var credentials = new AWS.SharedIniFileCredentials({profile: CONFIG.profile});
AWS.config.credentials = credentials;

let dynamo = new Dynamo.DocumentClient(CONFIG);

import Uuid = require("uuid/v4");

export function get_mechs_player(aid: string, uid:string, cb: (err, mechs) => void) {
    dynamo.get(
        {
            TableName:"StratisGameMechs", 
            Key: {"aid":aid, "uid":uid}
        }, 
        (err,data) => {
            if (data.Item) cb(null, data.Item.mechs)
            else cb(true, null)
        }
    );
}

export function get_mechs_area(aid: string, cb: (err, mechs: any[]) => void) {
    dynamo.query(
        {
            TableName:"StratisGameMechs", 
            ExpressionAttributeValues: {":a":aid},
            KeyConditionExpression: "aid = :a"
        }, 
        (err,data) => {
            if (data && (data.Count > 0)) cb(null, data.Items)
            else cb(true, null)
        }
    );
}

// NOTE: this fails if member is missing mech list to begin with!
// NOTE: this will append duplicate mechs, we should find a update design
export function add_mechs(aid: string, uid:string, mid: string,
    cb: (err, success) => void) {
    
        dynamo.update(
        {
            TableName:"StratisGameMechs",
            Key: {"aid":aid, "uid": uid},
            UpdateExpression: "SET mechs."+mid+" = :mech",
            ExpressionAttributeValues: {
                ":mech" : { "cost": 25000 }
            },
            ReturnValues: "UPDATED_NEW"
        }, 
        cb
    );
}

export function add_player(aid: string, uid:string,
    cb: (err, success) => void) {
    
        dynamo.update(
        {
            TableName:"StratisGameMechs",
            Key: {"aid":aid, "uid": uid},
            UpdateExpression: "SET mechs = :mechs",
            ExpressionAttributeValues: {
              ":mechs" : {}
            }
        }, 
        cb
    );
}

// recently registered user
export function mech_init(uid:string, cb?: (err, mid:string) => void) {
    let mid = "mi_" + rand();
    add_player("training", uid,
        (err, _) => {
            if (!err) add_mechs("training", uid, mid, (e, mech) => {
                if (cb) cb(e, mech.Attributes.mechs);
            });
        }
    );
}

function rand(): number {
    let high = 1e6;
    let low = 1e5 + 1;
    let r = Math.floor(Math.random() * (high - low) + low);
    return r
}
