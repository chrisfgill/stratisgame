import Dynamo = require("aws-sdk/clients/dynamodb");
import AWS = require("aws-sdk");

const CONFIG = require("./config.json").aws;
var credentials = new AWS.SharedIniFileCredentials({profile: CONFIG.profile});
AWS.config.credentials = credentials;

let dynamo = new Dynamo.DocumentClient(CONFIG);

export function get_token(uid: string, cb: (err, client) => void) {
    dynamo.get(
        {
            TableName:"StratisGameClients", 
            Key: {"uid":uid}
        }, 
        (err,data) => {
            if (data.Item) cb(null, { uid: data.Item.uid, priv: data.Item.priv } )
            else cb(true, null)
        }
    );
}

export function add_token(uid:string, priv: string, cb: (err, success) => void) {
    dynamo.put(
        {
            TableName:"StratisGameClients", 
            Item: {"uid":uid, "priv":priv}
        }, 
        cb
    );
}