const Noise = require("fast-simplex-noise").default;

export class Map {
    seed;

    constructor (seed: number) {
        this.seed = new Noise (
            { frequency: 0.01, max: 255, min: 0, octaves: 8 }
        );
    }

    gen ({x,y}): number {
        return this.seed.scaled([x, y]);// + this.seed.noise(y);
    }
}