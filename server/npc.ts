import { Name } from "./name";
const ORIGINS = require("./origins.json");
import { pick, rand } from "./rand";

export class Npc {
    name: string;
    heritage: string;
    makeup: Makeup;

    constructor () {
        let nb = new Name; // NOTE: we should move this out to a const builder
        this.name = nb.get_name();

        if (this.name.length > 9) this.heritage = "Bjor";
        else if (this.name.length < 5) this.heritage = "Rizu";
        else this.heritage = "Skyla";

        this.makeup = new Makeup(ORIGINS[this.heritage].makeup);
        
    }
}

class Makeup {
    peaceful = 0;
    spiritual = 0;
    pureblood = 0;
    vengeful = 0;
    cunning = 0;

    constructor (makeup: string[]) {
        let strong = makeup[pick(makeup)];
        for (let n in makeup) {
            let r;
            if (makeup[n] == strong) { r = rand(50, 100); }
            else { r = rand(1,100); }

            this[makeup[n]] = r;
        }

        for (let n in this) {
            if (this[n] < 1) this[n] = rand(1,50);
        }
    }
}