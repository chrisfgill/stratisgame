export function rand(low = 0, high = 1e6): number {
    let r = Math.floor(Math.random() * (high - low) + low);
    return r
}

export function pick (a: any[]): number {
    return rand(0,a.length);
}