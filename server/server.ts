import Express = require("express");
import Http = require("http");
import SocketIo = require("socket.io");
import Hmac = require("create-hmac");

import {get_token, add_token} from "./clients";
import {mech_init, get_mechs_player} from "./mechs";

import {Map} from "./map";

var clients = {};
const MAX_RPS = 2;

export class Server {
    app: Express.Application;
    http: Http.Server;
    io: SocketIO.Server;
    
    constructor () {
        this.app = Express();
        this.http = new Http.Server(this.app);
        this.io = SocketIo(this.http);

        this.app.use("/", Express.static(process.cwd()+"/build/client/"));

        this.app.get('/', function (req, res) {
            res.redirect("index.html")
        });

        this.io.on("connect", (socket) => {
            this.handler(socket);
        });

        this.http.listen(6063, () => {})
    }

    private handler(socket) {
        let auth_uid: string = null; // successfully authorized uid
        let ping_time = Date.now();
        let ping_tic = rand();
        const next_req_time = function() {return (Date.now() + MAX_RPS * 1000); }; // in ms
        let last_req_time = Date.now() - 1;

        // track rps, throttler
        const can_req = (uid?) : boolean => {
            if (last_req_time < Date.now()) {
                last_req_time = next_req_time();
                if (!uid) return true;
                else return (uid != null);
            }

            return false;
        };

        const on_auth = (cb?: () => void) => {
            console.log("auth", auth_uid);
            socket.emit("auth");
            setInterval(() => {
                ping_time = Date.now();
                ping_tic = rand();
                socket.emit("pinged", ping_tic);
            }, 1000*6);


            let map: Map = new Map(5); // seed 5
            let b = [];
            let size = 400;
            for (var x = 0; x < size; x++) {
                b[x] = [];
                for (var y = 0; y < size; y++) {
                    b[x][y] = map.gen({x: x, y: y});
                }
            }
            socket.emit("map", {w:size, h:size, a: b});


            if (cb) cb();
        };

        socket.on("register", (data) => {
            if (!clients[data.uid] && can_req()) {
                get_token(data.uid, (err,_) => {
                    if (err) {
                        add_token(data.uid, data.priv, (err,success) => {
                            if (success) {
                                auth_uid = data.uid;
                                clients[data.uid] = data;
                                clients[data.uid].socket = socket;
                                mech_init(data.uid, (err, mech: {}) => {
                                    if (!err) socket.emit("mech", mech);
                                });
                                on_auth();
                            }
                            else socket.disconnect();
                        });
                    }
                });
            }
            else socket.disconnect();
        });

        // process a session for auth
        socket.on("sid", (data) => {
            let start_auth = () => {
                if (clients[data.uid] && is_auth(clients[data.uid],data)) {
                    auth_uid = data.uid;
                    clients[data.uid].socket = socket;
                    on_auth();
                }
                else socket.disconnect(); // we won't spoil any current auths of client here
            };

            if (!clients[data.uid] && can_req()) {
                get_token(data.uid, (err,client_) => {
                    clients[data.uid] = client_;
                    start_auth();
                });
            }
            else start_auth();
        });

        socket.on("ponged", (tic) => {
            if (tic == ping_tic) {
                let delta = Date.now() - ping_time;
                ping_time = Date.now();
                socket.emit("delta", delta);
            }
        });
        socket.on("disconnect", () => {
            if (clients[auth_uid]) clients[auth_uid] = null;
        });

        socket.on("aid", (aid) => {
            if (can_req(auth_uid)) {
                console.log("aid",aid);
                get_mechs_player(aid, auth_uid, (err,mechs) => {
                    if (!err) socket.emit("aid", {player:mechs}); 
                });
            }
        })
    }
}

function is_auth (client, data): boolean {
    if (!client.priv) { console.error(client); return false }
    
    let hmac = Hmac("sha1", Buffer.from(client.priv));
    hmac.write(data.sid);
    hmac.end();

    let allow = true;
    let key = hmac.read();
    
    for (var i=0;i<20;i++) {
        if (key[i] != data.key.data[i]) {
            allow = false;
            break;
        }
    }

    return allow;
}

function rand(): number {
    let high = 1e6;
    let low = 1e5 + 1;
    let r = Math.floor(Math.random() * (high - low) + low);
    return r
}
