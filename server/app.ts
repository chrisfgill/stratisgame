import Express = require("express");
import {Name} from "./name";

const app = Express();
const nb = new Name;

app.get('/', (req, res) => res.send("nothing here now, shh."));
app.get('/name', (req, res) => res.send(nb.get_name()));

app.listen(6069);