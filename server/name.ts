import Chain from "markov-chains";
import fs = require("fs");
const rand = require("./rand").rand;


export class Name {
    src = [];
    constructor () {
        let src = fs.readFileSync("./build/server/names.txt", "utf8");
        this.src = src.split(/\r?\n/);
    }

    get_name(): string {
        let names = [];

        this.src.forEach(e => {
            let name = [];
            for (var i = 0; i < e.length; i++) {
                let c = e.charAt(i);
                name.push([c.toLowerCase()]);
            }

            names.push(name);
        });
        
        let chain = new Chain(names);

        let name = "";
        let is_cons = function (l) {
        let vowels = ["a", "e", "i", "o", "u"];
        for (var n in vowels) {
            if (l == vowels[n]) {
                return false;
            }
        }

        return true;
        };

        let build_name = function () {
            let walk = chain.walk();
            for (var n in walk) {
                let l = walk[n];
                if (name.length > 1) {
                    if (is_cons(l)) {
                        if (is_cons(name[name.length-1])) {
                            if (name.length > 2) {
                                if (is_cons(name[name.length-2])) {
                                    continue
                                }
                            }
                            else continue
                        }
                    }
                    else {
                        if (!is_cons(name[name.length-1])) {
                            if (name.length > 2) {
                                if (!is_cons(name[name.length-2])) {
                                    continue
                                }
                            }
                        }
                    }
                }

                name = name + l;
            }
        };

        while (name.length < 3) { build_name() }

        name = name.charAt(0).toUpperCase() + name.slice(1);
        if (name.length > 9) {
            let from = rand((name.length / 2) - 2, (name.length / 2) + 3);
            name = name.slice(0, from) + "-" + name.slice(from);
        }

        return name;
    }
}