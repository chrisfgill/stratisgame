import Uuid = require("uuid/v4");
import Hmac = require("create-hmac");

export class Client {
    socket;
    connected = false;
    uid: string;
    key: string; // session access key (hmac of sid and uid-priv)

    aid: string; // area id on the server that we are curr looking at

    constructor (io, events) {
        this.socket = io;

        // get or build sid
        var sid = window.sessionStorage.getItem("sid");
        if (!sid) {
            sid = rand().toString();
            window.sessionStorage.setItem("sid", sid);
        }

        // get or build uids
        this.uid = window.localStorage.getItem("uid");
        let registering = false;
        if (!this.uid) {
            registering = true;
            this.uid = Uuid();
            window.localStorage.setItem("uid", this.uid);
            window.localStorage.setItem("uid-priv", Uuid());

            window.localStorage.setItem("aid", "training");
        }

        let hmac = Hmac("sha1", Buffer.from(window.localStorage.getItem("uid-priv")));
        hmac.write(sid);
        hmac.end();
        this.key = hmac.read();



        
        this.socket.on("auth", () => {
            console.log('connected');
            this.connected = true;
            events.emit("connect");

            this.aid = window.localStorage.getItem("aid");
            setTimeout(() => {this.send("aid", this.aid)},2000);
        });

        this.socket.on("disconnect", () => {
            this.connected = false;
            console.error("disconnected");
            events.emit("disconnect");
        });

        this.socket.on("pinged", (tic) => {
            this.socket.emit("ponged", tic); 
        });

        this.socket.on("delta", (delta) => {
            events.emit("delta", delta);
        });

        this.socket.on("aid", (data) => {
            let obj: {aid,mechs} = <any>{};
            obj.mechs = data.player;
            obj.aid = window.localStorage.getItem("aid");
            events.emit("aid", obj);
        });

        this.socket.on("mech", (data) => {
            if (data) console.log("mech:", data) 
        });

        this.socket.on("map", (data) => {
            if (data) events.emit("map", data); 
        });

        this.socket.on("connect", () => {
            if (!registering)
                this.socket.emit("sid", { sid: sid, key: this.key, uid: this.uid });
            else {
                this.socket.emit("register", 
                                 { 
                    priv: window.localStorage.getItem("uid-priv"), 
                    uid: this.uid,
                    sid: sid,
                });
            }
        });
    }

    send (route: string, data, max_retries: number = 5) {
        if (!this.connected) {
            if (max_retries > 0) {
                max_retries--;
                setTimeout(() => { 
                    this.send(data, max_retries) 
                }, 1000); // retry in 1 second
            }
            else { this.socket.disconnect() }
        }
        else this.socket.emit(route, data);

        console.log("sent",route,data);
    }
}

function rand(): number {
    let high = 1e6;
    let low = 1e5 + 1;
    let r = Math.floor(Math.random() * (high - low) + low);
    return r
}
