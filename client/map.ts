export function build_map ({w,h,a}) {
    let b = new Uint8ClampedArray(w * h * 4);
    
    for(var x = 0; x < w; x++) {
        for(var y = 0; y < h; y++) {
            var pos = (y * w + x) * 4;
            b[pos] = a[x][y];
            b[pos+1] = a[x][y];
            b[pos+2] = a[x][y];
            b[pos+3] = 255;
        }
    }

    var canvas = document.createElement('canvas'),
    ctx = canvas.getContext('2d');

    canvas.width = w;
    canvas.height = h;

    var idata = ctx.createImageData(w, h);
    idata.data.set(b);
    ctx.putImageData(idata, 0, 0);

    var image = new Image();
    image.src = canvas.toDataURL();

    document.body.appendChild(image);
}