import {Client} from "./client";
import {Ui, Component} from "./ui";
import Events = require("events");
import Map = require("./map");

export class App {
    ui: Ui;
    client: Client;
    events: Events;

    constructor (io, target) {
        let events = new Events();
        this.events = events;

        this.ui = new Ui({target, events});
        this.client = new Client(io, events);

        setup_conn_comps(this);
        setup_ihc_comps(this);
        setup_mech_comps(this);

        setup_actions(this);

        events.on("map", (data) => {
            Map.build_map(data);
        });
    }

    send (route, data) {
        this.client.send(route, data);
    }
};

(window as any).App = function (io, target) { new App(io,target) };

function setup_actions(app: App) {

}

function setup_conn_comps (app: App) {

    // connection status display
    var net_status = new Component({
        name: "network_status",
        kind: "p", 
        html: "network connection status",
        css: ["icon"]
    });
    app.ui.attach(net_status);

    net_status.target.title = "connection status";
    app.events.on("connect", () => { 
        net_status.target.innerHTML = "&#x27b0;";
        net_status.target.title = "connection status: connected";
    });
    app.events.on("disconnect", () => { 
        net_status.target.innerHTML = "&#x270a;";
        net_status.target.title = "connection status: disconnected";
    });


    // ping times display 
    var net_delta = new Component({
        name: "network_delta",
        kind: "p", 
        html: "--ms",
        css: ["icon"]
    });
    app.ui.attach(net_delta);

    net_delta.target.title = "connection delta";
    app.events.on("delta", (delta) => { 
        net_delta.target.innerHTML = delta+"ms";
    });
    app.events.on("disconnect", () => { 
        net_delta.target.innerHTML = "--ms";
    });
}

function setup_ihc_comps (app:App) {
    var ihc = new Component({
        name: "ihc",
        kind: "div", 
        css: ["noshow"]
    });
    app.ui.attach(ihc);
    app.events.on("disconnect", () => ihc.noshow() );
    // hide for now
    //app.events.on("connect", () => ihc.show() );

    var ihc_tech = new Component({
        name: "ihc_tech",
        kind: "div"
    });
    ihc.append(ihc_tech.target);

    var ihc_buy = new Component({
        name: "ihc_buy",
        kind: "div",
        text: "Buy Tech"
    });
    ihc_tech.append(ihc_buy.target);

    var ihc_buy_btn = new Component({
        name: "ihc_buy_btn",
        actions:{ "onclick": () => console.log("buying") },
        kind: "button",
        text: "Purchase"
    });
    ihc_buy.append(ihc_buy_btn.target);
}

function setup_mech_comps (app:App) {
    var mechs = new Component({
        name: "mechs",
        kind: "div", 
        css: ["noshow"]
    });
    app.ui.attach(mechs);
    app.events.on("disconnect", () => mechs.noshow() );
    app.events.on("connect", () => mechs.show() );

    var aid = new Component({
        name: "aid",
        kind: "p"
    });
    mechs.append(aid.target);

    var mech = new Component({
        name: "mech",
        kind: "p",
        css: ["mech", "noshow"]
    });
    mechs.append(mech.target);

    var mechs_list = new Component({
        name: "mechs_list",
        kind: "ul"
    });
    mechs.append(mechs_list.target);


    app.events.on("aid", (data) => {
        aid.target.innerText = data.aid;

        // clear out
        while (mechs_list.target.firstChild) {
            mechs_list.target.removeChild(mechs_list.target.firstChild);
        }

        for (var mn in data.mechs) {
            var m = data.mechs[mn];

            var mc = new Component({
                name: "mechs_list_"+mn,
                kind: "li",
                text: mn,
                actions: { 
                    onclick: function() {
                        mech.clear();

                        var c = new Component({
                            name: "mech_name",
                            kind: "p",
                            text: "name: " + mn
                        });
                        mech.append(c.target);

                        var c = new Component({
                            name: "mech_cost",
                            kind: "p",
                            text: "cost: " + m.cost
                        });
                        mech.append(c.target);
                        
                        mech.show();
                    } 
                }
            });
            mechs_list.append(mc.target);
        }
    });
}