export class Ui {
    ui: HTMLElement;
    components: {string:Component} = ({} as any);
    parent: Ui;

    constructor({target, events}) {
        if (target) this.ui = document.getElementById(target);
        if (!this.ui) {
            this.ui = document.createElement("div");

            if (target) this.ui.id = target;
        }
    }

    attach (comp: Component) {
        this.ui.appendChild(comp.target);
        this.components[comp.name] = comp;
    }

    parent_set(parent: Ui) {
        parent.ui.appendChild(this.ui);
        this.parent = parent;
    }

    // clear out entire child list
    clear () {
        for (var comp in this.components) {
            this.components[comp].clear();
        }
    }
}

export class Component {
    target: HTMLElement;
    name: string;

    constructor(
        { 
            name,
            actions = {},
            kind = "div", 
            text = "", html = "", 
            css = <Array<string>>[] 
        } ) {
        this.name = name;

        this.target = document.createElement(kind);
        this.target.id = name;
        
        if (text) this.target.innerText = text;
        else if (html) this.target.innerHTML = html;

        for (var i in actions)
            this.target[i] = actions[i];

        for (var i in css)
            this.target.classList.add(css[i]);
    }

    show () {
        this.target.classList.remove("noshow");
    }
    noshow () {
        this.target.classList.add("noshow");
    }

    // only adds dom child
    append(ele: HTMLElement) {
        this.target.appendChild(ele)
    }

    clear() {
        while (this.target.firstChild) {
            this.target.removeChild(this.target.firstChild);
        }
    }
}