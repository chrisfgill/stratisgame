# Mining

## Basic premise

- Interstellar Habitation Consortium (IHC) will provide funding to mining companies.
- On starting, minimal funding will be provided based on location and randomness.
- Minimal funding will be enough for a couple of mining machines.
- Player will have to decide what machines are a good starting point.

## Technology

- Communication is based on satellite reach and coverage
- Player can rent sat time
- Mining units
- Cargo holds
- Carriers
- Scouts and scanners
- Energy types (solar, electrostatic, wind, geothermal, hydrogen fuel cell)