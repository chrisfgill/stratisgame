# what keeps users going

## road blocks

- unending completion
- impossible max growth
- steps of gameplay that operate in a circle

## associated production costs of mining

- mech repairs
- cargo transit
- container storage
- survey fees

## resource allocation

- zone resource depletion
- variable resource regneration times
- long term mining affects

## law of diminishing returns

- tiers of growth become impossibly difficult
- hassle of gigantic operations
- ui concerns as well